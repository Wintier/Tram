document.getElementById('dynamicButton').removeAttribute('onclick')
document.getElementById('dynamicButton').setAttribute('onclick', "window.location.replace('index.html')")

var projInfo = require('../../../projInfo.json')
let authors = projInfo.authors

console.log(authors)

for (var i = 0, len = authors.length; i < len; i++) {
  var newColumn = document.createElement('div')
  newColumn.classList.add('column')
  newColumn.setAttribute('id', 'authorField-' + i)
  newColumn.setAttribute('onclick', "const {clipboard} = require('electron'); clipboard.writeText('" + authors[i].url + "')")
  document.getElementById('contributorsListing').appendChild(newColumn)
  var para = document.createElement('p')
  var node = document.createTextNode(authors[i].name)
  para.appendChild(node)
  document.getElementById('authorField-' + i).appendChild(para)
}

var thanksPara = document.createElement('p')
var thanksNode = document.createTextNode(projInfo.thanks)
thanksPara.appendChild(thanksNode)
document.getElementById('overwriteWithThanks').appendChild(thanksPara)
