let os = require('os')
let projInfo = require('./projInfo.json')
let logKit = require('./logKit/index')
let requestingModule = 'printSystemInfo'
let tabID = 'N/A'
let activity = 'Printing system info for debugging'

logKit(requestingModule, tabID, activity, 'info', 'Platform details: ' + os.arch() + ' ' + os.type() + ' ' + os.platform() + ' ' + os.release() + ' Node ' + process.version)
logKit(requestingModule, tabID, activity, 'info', 'Application details: ' + projInfo.vendor + ' ' + projInfo.name + ' ' + projInfo.release + ' ' + projInfo.codename + ' ' + projInfo.version)
logKit(requestingModule, tabID, activity, 'info', 'Memory details: ' + os.freemem() + ' available, ' + os.totalmem() + ' in the system.')
logKit(requestingModule, tabID, activity, 'info', 'Printing CPU details..')
console.log(os.cpus())
