/* global tabKit cache logKit */
let requestingModule = 'tabKit.create'
let action = 'Tab Creation'
let projInfo = require('../projInfo.json')
module.exports = function (url, focus) {
  var tabID = tabKit.genTabID()
  var tab = document.createElement('webview')
  tab.setAttribute('src', url)
  tab.setAttribute('id', tabID)
  tab.setAttribute('useragent', projInfo.vendor.replace(/ /g, '') + projInfo.name.replace(/ /g, '') + '/' + projInfo.release.replace(/ /g, '') + ' Mozilla/5.0 Chromium/' + process.versions['chrome'])
  document.getElementById('viewport').appendChild(tab)
  cache.set('tabInfo-' + tabID, {})
  tabKit.watchEvents(tabID)
  tabKit.changeFocus(tabID, focus)
  logKit(requestingModule, tabID, action, 'info', 'Changed viewport focus to this tab')
}
