/* global tabKit cache uiKit */
module.exports = function (tabID) {
  var tabInfo = cache.get('tabInfo-' + tabID)

  document.getElementById(tabID).addEventListener('new-window', (e) => {
    const protocol = require('url').parse(e.url).protocol
    switch (protocol === 'http:' || protocol === 'https:' || protocol === 'tram:') {
      case true:
        tabKit.create(e.url, 'active')
        break
    }
  })

  document.getElementById(tabID).addEventListener('enter-html-full-screen', (e) => {
    tabInfo.fullscreen = true
    // I don't know why this needs to be called two times, but it does
    uiKit.fullScreen()
    uiKit.fullScreen()
  })

  document.getElementById(tabID).addEventListener('leave-html-full-screen', (e) => {
    tabInfo.fullscreen = false
    // same story
    uiKit.fullScreen()
    uiKit.fullScreen()
  })

  document.getElementById(tabID).addEventListener('close', (e) => {
    tabKit.remove(tabID)
  })

  document.getElementById(tabID).addEventListener('page-title-updated', (e) => {
    tabInfo.title = e.title
    cache.set('tabInfo-' + tabID, tabInfo)
  })

  document.getElementById(tabID).addEventListener('did-navigate-in-page', (e) => {
    tabInfo.URL = e.url
    cache.set('tabInfo-' + tabID, tabInfo)
  })

  document.getElementById(tabID).addEventListener('did-navigate', (e) => {
    tabInfo.URL = e.url
    cache.set('tabInfo-' + tabID, tabInfo)
  })

  document.getElementById(tabID).addEventListener('did-start-loading', (e) => {
    tabInfo.loading = true
    tabInfo.favicons = [] // make sure that we're not using the previous site's favicon if the new page doesn't have any
    cache.set('tabInfo-' + tabID, tabInfo)
  })

  document.getElementById(tabID).addEventListener('did-stop-loading', (e) => {
    tabInfo.loading = false
    cache.set('tabInfo-' + tabID, tabInfo)
  })

  document.getElementById(tabID).addEventListener('page-favicon-updated', (e) => {
    tabInfo.favicons = e.favicons
    cache.set('tabInfo-' + tabID, tabInfo)
  })
}
