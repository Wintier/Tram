/* global tabKit cache */
module.exports = function () {
  let webHub = document.getElementById('webHub')
  switch (webHub.classList.contains('webHubActive')) {
    case true:
      webHub.classList.remove('webHubActive')
      break
    case false:
      document.getElementById('omnibox').value = ''
      document.getElementById('omnibox').value = cache.get('tabInfo-' + tabKit.currentActiveTab())['URL']
      switch (cache.get('tabInfo-' + tabKit.currentActiveTab())['URL'] === 'tram-pub://webHubDummy') {
        case true:
          document.getElementById('omnibox').value = ''
          break
      }
      document.getElementById('omnibox').focus()
      document.getElementById('omnibox').select()
      webHub.classList.add('webHubActive')
      break
  }
}
